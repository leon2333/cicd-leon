const passport = require('passport');
const Strategy = require('passport-http-bearer').Strategy;
const User = require('./models/user');
const config = require('./config');
let mongoose = require('mongoose');

module.exports = function(passport) {
    var mongodbUri= 'mongodb+srv://leon:liang369369@wit-donation-cluster-lovf9.mongodb.net/foodhub?retryWrites=true&w=majority'
    mongoose.connect(mongodbUri,{useNewUrlParser:true});
    mongoose.set('useFindAndModify',false);

    passport.use(new Strategy(
        function(token, done) {
            User.findOne({
                token: token
            }, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false);
                }
                return done(null, user);
            });
        }
    ));
};
